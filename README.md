datalife_device
===============

The device module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-device/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-device)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
