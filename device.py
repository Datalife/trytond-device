# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from datetime import datetime
from trytond.model import ModelView, ModelSQL, fields, DeactivableMixin
from trytond.pool import Pool
from trytond.pyson import Id
from trytond.rpc import RPC
from trytond.exceptions import UserError
from trytond.i18n import gettext


class DeviceType(ModelView, ModelSQL):
    """"Device Type"""
    __name__ = 'device.type'

    name = fields.Char('Name', required=True)


class Device(ModelView, ModelSQL, DeactivableMixin):
    """Device"""
    __name__ = 'device'

    code = fields.Char('Code', required=True)
    name = fields.Char('Name', required=True)
    type = fields.Many2One('device.type', 'Type', required=True)
    sequence = fields.Many2One('ir.sequence', 'Sequence',
        domain=[
            ('sequence_type', '=', Id('device', 'sequence_type_device'))
        ])


class DeviceRequest(ModelView, ModelSQL):
    """Device Request"""
    __name__ = 'device.request'

    code = fields.Char('Code')
    device = fields.Many2One('device', 'Device', required=True)
    date_sent = fields.DateTime('Date Sent')
    request = fields.Char('Request')
    response = fields.Char('Response')
    value = fields.Char('Value')

    @classmethod
    def __setup__(cls):
        super(DeviceRequest, cls).__setup__()
        cls.__rpc__.update({
                'register_request': RPC(readonly=False),
                })

    @classmethod
    def default_date_sent(cls):
        return datetime.now()

    @classmethod
    def create(cls, vlist):
        Device = Pool().get('device')

        for vals in vlist:
            if vals.get('device') and vals.get('code', None) is None:
                device = Device(vals['device'])
                if device.sequence:
                    vals['code'] = device.sequence.get()
        return super().create(vlist)

    def compute_value(self):
        pos = self.response.find('*')
        if pos > 0:
            self.value = self.response[pos - 6:pos - 2]

    @classmethod
    def register_request(cls, records):
        pool = Pool()
        Device = pool.get('device')
        for record in records:
            devices = Device.search([('code', '=', record['device_code'])])
            if not devices:
                raise UserError(gettext(
                    'device.msg_device_request_unknown_device'),
                    device=record['device_code'])
            device = devices[0]
            request = DeviceRequest()
            request.device = device
            request.request = record['request']
            request.response = record['response']
            request.save()
            request.compute_value()
            request.save()
